
STR_NICKNAME="BRT"
MERCHANT_NO="261064828"

STR_REFERENCE="eCard Top-up"


STROPENTIMEOUT="180"
HEADERS={'Content-Type'=> 'application/json','timeout'=>STROPENTIMEOUT, 'open_timeout'=>STROPENTIMEOUT}


CLIENTID="i2ANQ0lLLxzLVeycm+s6u/P86GXPttoJAORHoxexomqe9Th5kpo+0lVjUKGSlFovCx41wRb7mxGJvYineBdqew=="
SECRETKEY="QiJ8fhF47EDy8Hdi5iq9Vu+boGu8o5thXNhW1Ji1kYCCLdnf03GBoXa6Ip8qWXxy1KQnlxHp6tGXmswNGIMFng=="


configure :development do
  
  CONF = YAML.load(File.open(File.expand_path('./config/config.yml')))
  
  
  dbhost=CONF['aayalolo_ussd_db']["db_host"]
  dbuser=CONF['aayalolo_ussd_db']["db_username"]
  dbpass=CONF['aayalolo_ussd_db']["db_password"]
  dbname=CONF['aayalolo_ussd_db']["db_name"]
  #dbport=CONF['aayalolo_ussd_db']["port"]
  
  
  ActiveRecord::Base.establish_connection(
    adapter:  'mysql2',
    host:     dbhost,
    username: dbuser,
    password: dbpass,
    database: dbname
  )
  
end



def mobileMoneyReq(mobile_number, amount, card_no, mno)
  u_code=genUniqueCode
  url="https://10.57.13.121:8201"
  url_endpoint='/debitCustomerWallet'
  
  conn = Faraday.new(:url => url, :headers => HEADERS, :ssl => {:verify => false}) do |faraday|   
    #faraday.request  :url_encoded             # form-encode POST params
    ##faraday.options.timeout = 5           # open/read timeout in seconds
    ##faraday.options.open_timeout = 2      # connection open timeout in seconds
    faraday.response :logger                  # log requests to STDOUT
    faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
  end

ts=Time.now.strftime("%Y-%m-%d %H:%M:%S")

saveTopupRequest(mobile_number, card_no, amount, mno, u_code)

payload={ 
  :merchant_number=> MERCHANT_NO,
  :customer_number=> mobile_number,
  :amount=> amount,
  :reference=> STR_REFERENCE,
  :exttrid=> u_code,
  :nickname=>STR_NICKNAME,
  :ts=>ts 
  }


json_payload=JSON.generate(payload)
msg_endpoint="#{url_endpoint}#{json_payload}"

def computeSignature(secret, data)
  digest=OpenSSL::Digest.new('sha256')
  signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
  return signature
end
 
signature=computeSignature(SECRETKEY, msg_endpoint)

begin
res=conn.post do |req|
  req.url url_endpoint
  req.options.timeout = 30           # open/read timeout in seconds
  req.options.open_timeout = 30      # connection open timeout in seconds
  req["Authorization"]="#{CLIENTID}:#{signature}"
  req.body = json_payload
end

puts 
puts "Alpha"
if res.status.to_s=="200" then
  puts
  puts "Beta"
  puts
  saveMpaymentReq(MERCHANT_NO, mobile_number, amount, STR_REFERENCE, u_code, STR_NICKNAME, ts, mno)
end
puts
puts "Response: #{res.status} #{res.body}"
puts

rescue Faraday::SSLError
  puts
  puts "There was a problem sending the https request..."
  puts
rescue Faraday::TimeoutError
  puts "Connection timeout error"
end

end


def genUniqueCode
  time=Time.new
  #randval=rand(999).to_s.center(3, rand(9).to_s)
  strtm=time.strftime("%y%m%d%L%H%M")
  
  #p strtm+randval
  return strtm
end

def savePaymentStatus(trxn_status, transId, trxn_ref, trxn_msg)
  rec=PaymentStatus.create!(trans_status: trxn_status, trans_id: transId, trans_ref: trxn_ref, trans_msg: trxn_msg)
end

def saveTopupRequest(mobile, card_no, amt, mno, exttrId)
  puts
  puts "Inside topup routine..."
  puts
  rec=TopupRequest.create!(mobile_number: mobile, card_number: card_no, topup_amt: amt, mm_operator: mno, exttrid: exttrId)
  puts rec  
end

def saveMpaymentReq(merchant_no, customer_no, amt, ref, exttrId, nickname, ts, mno)
  rec=MpaymentReq.create!(merchant_number: merchant_no, customer_number: customer_no, amount: amt, reference: ref, exttrid: exttrId, nickname: nickname, req_ts: ts, mm_operator: mno)
  puts rec
end

def saveUSSD(sequence, end_of_session, session_id, service_key, mobile_number, ussd_body="")
  rec=UssdRequest.create!(mobile_number: mobile_number, sequence: sequence, end_of_session: end_of_session, session_id: session_id, service_key: service_key, ussd_body: ussd_body)
end

def sendmsg(senderID, receipient, txtmsg)
  strUser="tester"
  strPass="foobar"
  strIP="localhost"
  strHostPort="13013"
  
  
  time=Time.new
  msgID=strtm=time.strftime("%y%m%d%H%M%S%L")

  strDlrUrl="http://#{strIP}:#{strHostPort}/Dlr?msgID=#{msgID}&smsc_reply=%A&sendsms_user=%n&ani=%P&bni=%p&dlr_val=%d&smsdate=%t&smscid=%i&charset=%C&mclass=%m&bmsg=%b&msg=%a"
    
  uri=URI("http://#{strIP}:#{strHostPort}/cgi-bin/sendsms")
  uri.query=URI.encode_www_form([["username", strUser], ["password", strPass], ["charset", "UTF-8"], ["to", receipient], ["dlr-mask", "31"], ["from", senderID], ["text", txtmsg], ["smsc","airtel"],["dlr-url", strDlrUrl]])
  
    
    
  res = Net::HTTP.get_response(uri)
  puts res.code
  puts res.body
  
  p resp=Msg.create(msg_id: senderID, phone_number: receipient, msg: txtmsg, resp_code: res.code, resp_desc: res.body, status: 1)
end

