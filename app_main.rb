require 'sinatra'
require 'sinatra/activerecord'
require 'yaml'
require 'mysql2'
require 'active_record'
require 'json'
require 'net/http'
require 'savon'
require 'faraday'
require 'digest/md5'
require 'openssl'
require 'nokogiri'
require './model/models'
require './requests'

STRRESPONSE = "RESPONSE"
STRFALSE = "FALSE"
STRTRUE = "TRUE"
SEQUENCE = "SEQUENCE"

STR_CARD_NO="Please provide your eCard number"
STR_AMOUNT="Top-up amount (GHC)"
STR_CANCEL="You have chosen to cancel the top-up request. Thank you."
STR_REQ_PROGRESS="Your top-up request in progress. Please wait..."

STR_AIRTEL="AIR"
STR_MTN="MTN"
STR_TIGO="TIG"
STR_VODA="VOD"

senderID="BRT"

post '/' do
  request.body.rewind
  @payload=JSON.parse(request.body.read)
  json_vals = @payload
  sequence=json_vals['SEQUENCE']
  end_session=json_vals['END_OF_SESSION']
  sessionID=json_vals['SESSION_ID']
  service_key=json_vals['SERVICE_KEY']
  mobile_number=json_vals['MOBILE_NUMBER']
  ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
  process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
end

def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
  saveUSSD(sequence, end_of_session, sessionID, service_key, mobile, ussd_body)

  if sequence=="0"
    s_params = Hash.new

    strMainMenu="Bus Transit Service: \n\n1. Top-up eCard \n"
    #strMainMenu<<"2. Buy Ticket\n"
    strMainMenu<<"2. Purchase eCard"

    s_params['SEQUENCE']=sequence
    s_params['REQUEST_TYPE']=STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY']=strMainMenu

    p s_params.to_json
  elsif sequence=="1" then
    if ussd_body=="1" then
      purchaseTopup("1", sequence, sessionID, mobile, service_key, ussd_body)
    elsif ussd_body=="2"then
      checkBalance("1", sequence, sessionID, mobile, service_key, ussd_body)
    end
  elsif sequence=="2" then
    selMenu=UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,
    sessionID, mobile, '1').select(:ussd_body)[0][:ussd_body]

    if selMenu=="1" then
      purchaseTopup("2", sequence, sessionID, mobile, service_key, ussd_body)
    end
  elsif sequence=="3" then
    selMenu=UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,
    sessionID, mobile, '1').select(:ussd_body)[0][:ussd_body]

    if selMenu=="1" then
      purchaseTopup("3", sequence, sessionID, mobile, service_key, ussd_body)
    end
  elsif sequence=="4" then
    selMenu=UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,
    sessionID, mobile, '1').select(:ussd_body)[0][:ussd_body]

    if selMenu=="1" then
      purchaseTopup("4", sequence, sessionID, mobile, service_key, ussd_body)
    end
  end
end

def purchaseTopup(step, sequence, sessionID, mobile, service_key, ussd_body)
  t_params=Hash.new

  strMainOption=ussd_body

  if step=='1' then
    strMainMenu=STR_AMOUNT
    t_params['END_OF_SESSION']=STRFALSE
  elsif step=='2' then
    strMainMenu=STR_CARD_NO
    t_params['END_OF_SESSION']=STRFALSE
  elsif step=='3' then
    topUpAmt=UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,
    sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]

    v_card_no=ussd_body

    strMainMenu="Top-up eCard: \n\n"
    strMainMenu<<"Top-up Amount: GHC #{topUpAmt}\n"
    strMainMenu<<"Card No.: #{v_card_no}\n"
    strMainMenu<<"\n1. Proceed \n2. Cancel"

    t_params['END_OF_SESSION']=STRFALSE
  elsif step=='4' then
    if ussd_body=='1' then
      topUpAmt=UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,
      sessionID, mobile, '2').select(:ussd_body)[0][:ussd_body]

      card_no=UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,
      sessionID, mobile, '3').select(:ussd_body)[0][:ussd_body]

      #saveTopupRequest(mobile, card_no, topUpAmt, STR_AIRTEL)
      Thread.new{
        mobileMoneyReq(mobile, topUpAmt, card_no, STR_AIRTEL)
      }

      strMainMenu=STR_REQ_PROGRESS
      t_params['END_OF_SESSION']=STRTRUE
    else
      strMainMenu=STR_CANCEL
      t_params['END_OF_SESSION']=STRTRUE
    end
  end

  t_params['SEQUENCE']=sequence
  t_params['REQUEST_TYPE']=STRRESPONSE
  t_params['USSD_BODY']=strMainMenu

  p t_params.to_json
end

def checkBalance(step, sequence, sessionID, mobile, service_key, ussd_body)
  t_params=Hash.new

  strMainOption=ussd_body

  if ussd_body.to_s=="2"
    strMainMenu="Sorry, balance check is not available yet"
    t_params['END_OF_SESSION']=STRTRUE
  end
  t_params['SEQUENCE']=sequence
  t_params['REQUEST_TYPE']=STRRESPONSE
  t_params['USSD_BODY']=strMainMenu

  p t_params.to_json
end

post '/reqCallback' do
  request.body.rewind
  req = JSON.parse request.body.read

  trans_id=req["trans_id"]
  trans_status=req["trans_status"]
  trans_ref=req["trans_ref"]
  message=req["message"]

  t=MpaymentReq.where(exttrid: trans_ref).select(:exttrid).count
  if t.to_i==1 then
    savePaymentStatus(trans_status, trans_id, trans_ref, message)

    res=MpaymentReq.where(exttrid: trans_ref).select(:customer_number, :amount)[0]
    txtmsg="Your payment of GHC #{res.amount}, for eCard top-up, was successful"

    if trans_status.to_s=="200"
      sendmsg(senderID, res.customer_number, txtmsg)
    else
      sendmsg(senderID, res.customer_number, "Sorry, your payment was unsuccessful")
    end
    resp={:resp_code=>"00", :resp_desc=>"Success"}
  else
    resp={:resp_code=>"01", :resp_desc=>"Failure"}
  end
  return resp.to_json
end

